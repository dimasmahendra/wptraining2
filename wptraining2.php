<?php
/*
Function Name: your_prefix_get_meta_box
Description: Creating metabox with metabox.io plugins
Version: 1.0
Author: Dimas Mahendra Kusuma
*/
function your_prefix_get_meta_box( $meta_boxes ) {
	$prefix = 'prefix-';

	$meta_boxes[] = array(
		'id' => 'untitled',
		'title' => esc_html__( 'Other information', 'metabox-team-member' ),
		'post_types' => array( 'post', 'page', 'team_members'),
		'context' => 'advanced',
		'priority' => 'default',
		'autosave' => false,
		'fields' => array(
			array(
				'id' => $prefix . 'position',
				'type' => 'text',
				'name' => esc_html__( 'Position', 'metabox-team-member' ),
			),
			array(
				'id' => $prefix . 'email',
				'name' => esc_html__( 'Email', 'metabox-team-member' ),
				'type' => 'email',
			),
			array(
				'id' => $prefix . 'phone_number',
				'type' => 'number',
				'name' => esc_html__( 'Phone Number', 'metabox-team-member' ),
			),
			array(
				'id' => $prefix . 'webiste',
				'type' => 'url',
				'name' => esc_html__( 'URL', 'metabox-team-member' ),
			),
			array(
				'id' => $prefix . 'image_advanced_6',
				'type' => 'image_advanced',
				'name' => esc_html__( 'Image', 'metabox-team-member' ),
			),
		),
	);

	return $meta_boxes;
}
add_filter( 'rwmb_meta_boxes', 'your_prefix_get_meta_box' );

/*
Plugin Name: Team Members
Description: Custom post type for Team Member.
Version: 1.0
Author: Dimas Mahendra Kusuma
*/
add_action( 'init', 'create_team_member' );

function create_team_member() {
    register_post_type( 'team_members',
        array(
            'labels' => array(
                'name' => 'Team Members',
                'singular_name' => 'Team Members',
                'add_new' => 'Add New',
                'add_new_item' => 'Add New Team Members',
                'edit' => 'Edit',
                'edit_item' => 'Edit Team Members',
                'new_item' => 'New Team Members',
                'view' => 'View',
                'view_item' => 'View Team Members',
                'search_items' => 'Search Team Members',
                'not_found' => 'No Team Members Reviews found',
                'not_found_in_trash' => 'No Team Members found in Trash',
                'parent' => 'Parent Team Members'
            ),
 
            'public' => true,
            'menu_position' => 15,
            'supports' => array( 'title', 'editor', 'comments', 'thumbnail'),
            'taxonomies' => array( '' ),
            'menu_icon' => plugins_url( 'images/movie.png', __FILE__ ),
            'has_archive' => true
        )
    );
}

/*
Shortcut Name: Team Members Shortcut
Description: Shortcut Team members
Version: 1.0
Author: Dimas Mahendra Kusuma
*/
add_shortcode('test', 'team_member_page');

function team_member_page(){
	global $wpdb;
  $post = $wpdb->get_results( "SELECT * FROM `wp_latihanposts` WHERE post_type like '%team_members%' AND post_title not like '%Auto%Draft%' AND post_status like '%publish%'", ARRAY_A);
  foreach ($post as $key => $value) {
    $post_meta[] = get_post_meta($value['ID']); 
  }   
  ?>
  <div class="wrap">
      <h2>Team Member</h2>
        <table id="tabelTesti" class="wp-list-table widefat fixed striped pages" cellspacing="0" border="1" width="100%">
            <thead>
              <tr>
                <th>No</th>
                <th>Position</th>  
                <th>Email</th>  
                <th>Phone Number</th>  
                <th>Website</th>  
                <th>Images</th>       
              </tr>
            </thead>
            <tbody>
                <?php
                  $no = 1;            
                  foreach ($post_meta as $row)
                  { ?>
                        <tr align="center">
                            <td><?php echo $no;$no++; ?></td>
                            <td><?php echo $row['prefix-position'][0]; ?></td> 
                            <td><?php echo $row['prefix-email'][0]; ?></td> 
                            <td><?php echo $row['prefix-phone_number'][0]; ?></td> 
                            <td><?php echo $row['prefix-webiste'][0]; ?></td> 
                            <td><img src="<?php echo wp_get_attachment_url( $row['prefix-image_advanced_6'][0] ); ?>"></td> 
                        </tr>
                <?php } ?>            
            </tbody>              
        </table>
    </div>
<?php }
?>